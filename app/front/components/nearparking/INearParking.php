<?php

namespace FrontModule\Components;

interface INearParking
{
    /**
     * @param string $googleApiKey
     *
     * @return NearParking
     */
    public function create(string $googleApiKey): NearParking;
}