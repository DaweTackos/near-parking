<?php

namespace FrontModule\Components;

use Nette;
use Exception;
use Nette\Http\Url;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\Button;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\TextBase;
use Nette\Forms\Controls\RadioList;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\Controls\MultiSelectBox;

class NearParking extends Nette\Application\UI\Control
{

    const API_BASE_URL = 'api/near-parking';

    /** @var Url */
    private $apiUrl;

    /** @var string */
    private $googleMapsUrl = '';

    /** @var array */
    private $results = [];

    /** @var string */
    private $googleApiKey;


    public function __construct(string $googleApiKey)
    {
        parent::__construct();

        $this->apiUrl = new Url;
        $this->googleApiKey = $googleApiKey;
    }


    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/default.latte');
        $template->apiUrl = $this->apiUrl->getAbsoluteUrl();
        $template->googleMapsUrl = $this->googleMapsUrl;
        $template->results = $this->results;

        $template->render();
    }


    protected function createComponentNearParking(): Form
    {
        $form = new Form;

        $form->addRadioList('position', 'Okruh hledání', [
            'ip'  => sprintf('Vaše IP adresa'),
            'gps' => 'Poloha GPS'
        ])->setDefaultValue('ip')
          ->addCondition($form::EQUAL, 'gps')
            ->toggle('gps-latitude')
            ->toggle('gps-longitude')->endCondition();
        $form->addText('longitude', 'GPS výška')
            ->setOption('id', 'gps-longitude')
            ->addConditionOn($form['position'], Form::EQUAL, 'gps')
                ->addRule(Form::FLOAT, 'Zadejte číselnou hodnotu gps výšky.')
                ->setRequired('Vyplňtě GPS výšku');
        $form->addText('latitude', 'GPS šířka')
            ->setOption('id', 'gps-latitude')
            ->addConditionOn($form['position'], Form::EQUAL, 'gps')
                ->addRule(Form::FLOAT, 'Zadejte číselnou hodnotu gps šířky.')
                ->setRequired('Vyplňtě GPS šířku');
        $form->addText('byName', 'Název');
        $form->addSelect('sort', 'Řazení', [
            'freePlaces'  => 'počet míst - vzestupně',
            '-freePlaces' => 'počet míst - sestupně',
            '-distance'   => 'vzdálenost - vzestupně',
            'distance'    => 'vzdálenost - sestupně',
        ]);

        $form->addSelect('perPage', 'Počet na stránku', [
            5  => 5,
            10 => 10,
            15 => 15,
            20 => 20,
        ]);

        $form->addText('page', 'Stránka')
            ->setDefaultValue(1)
            ->setRequired(true)
            ->addRule(Form::NUMERIC, 'Stránka musí být celé číslo.');

        $form->addSubmit('submit', 'Odeslat');

        $form->onSuccess[] = [$this, 'nearParkingFormSucceeded'];

        $this->setupBootstrapFormRendering($form);

        return $form;
    }


    public function nearParkingFormSucceeded(Form $form,
                                             $values)
    {
        $url = $this->apiUrl;

        $hostUrl = $this->getPresenter()->getHttpRequest()->getUrl();
        $url->setScheme($hostUrl->getScheme());
        $url->setHost($hostUrl->getHost() . $hostUrl->getPath() .  self::API_BASE_URL);

        if ($values->position === 'ip') {
            $url->setPath(sprintf('ip'));
        } elseif ($values->position === 'gps') {
            $url->setPath(sprintf('gps/%s,%s', $values->longitude, $values->latitude));
        }

        if (!empty($values->byName)) {
            $url->setQueryParameter('name', urlencode($values->byName));
        }

        $url->setQueryParameter('page', $values->page);
        $url->setQueryParameter('perPage', $values->perPage);

        $url->setQueryParameter('sort', $values->sort);

        try {
            $apiResult = file_get_contents($url->getAbsoluteUrl());
            $apiResult = json_decode($apiResult);

            if ($apiResult->status === 'ok') {
                $this->results = $apiResult->parkingList;
                $this->googleMapsUrl = $this->createStaticGoogleMapsUrl($apiResult->parkingList,
                                                                        $apiResult->fromGps->latitude,
                                                                        $apiResult->fromGps->longitude);
            }

            $this->flashMessage('Vyfiltrováno.', 'success');
        } catch(Exception $e) {
            $this->flashMessage('Při filtrování došlo k chybě.', 'danger');
        }
    }


    private function createStaticGoogleMapsUrl(array $results,
                                               float $latitude,
                                               float $longitude,
                                               int $width = 600,
                                               int $height = 500): string
    {
        $url = new Url("https://maps.googleapis.com/maps/api/staticmap");
        $url->setQueryParameter('size', sprintf('%sx%s', $width, $height));
        $url->setQueryParameter('maptype', 'roadmap');
        $url->setQueryParameter('key', $this->googleApiKey);

        $url = (string)$url;
        foreach ($results as $result) {
            $url .= sprintf('&markers=color:blue|label:P|%s,%s', $result->latitude, $result->longitude);
        }

        $url .= sprintf('&markers=color:red|label:U|%s,%s', $latitude, $longitude);

        return $url;
    }


    private function setupBootstrapFormRendering(Form &$form)
    {
        // TODO: create separate renderer
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-9';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-9 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

        $form->getElementPrototype()->class('form-horizontal');
        foreach ($form->getControls() as $control) {
            if ($control instanceof Button) {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            } elseif ($control instanceof TextBase || $control instanceof SelectBox || $control instanceof MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Checkbox || $control instanceof CheckboxList || $control instanceof RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }
    }

}

