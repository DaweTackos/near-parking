<?php

namespace FrontModule\Presenters;

use Nette;
use FrontModule\Components\NearParking;
use FrontModule\Components\INearParking;


class ParkingPresenter extends Nette\Application\UI\Presenter
{

    /** @var INearParking @inject */
    public $nearParking;


    public function createComponentNearParking(): NearParking
    {
        $googleApiKey = $this->context->getParameters()['googleApiKey'];

        return $this->nearParking->create($googleApiKey);
    }

}
