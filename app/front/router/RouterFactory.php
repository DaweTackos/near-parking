<?php

namespace FrontModule;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use NearParking\RouterFactory AS NearParkingRouterFactory;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

        $router[] = (new NearParkingRouterFactory)->createRouter();

        $router[] = $frontRouter = new RouteList('Front');

		$frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Parking:default');

		return $router;
	}
}
