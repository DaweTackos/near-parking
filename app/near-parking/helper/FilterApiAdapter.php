<?php

namespace NearParking\Helper;


class FilterApiAdapter
{
    /**
     * Create filter item from api parameter.
     *
     * @param string $column
     * @param string $value
     *
     * @return array
     */
    public function createFilterItem(string $column,
                                     string $value): array
    {
        return [
            'column' => $column,
            'value'  => urldecode($value),
        ];
    }
}