<?php

namespace NearParking\Helper;


class IPLocator
{
    public function getGpsFromActualIP(): array
    {
        $locationData = json_decode(file_get_contents('http://ip-api.com/json'));

        return [
            'latitude'  => $locationData->lat,
            'longitude' => $locationData->lon,
        ];
    }
}