<?php

namespace NearParking\Helper;

class SortApiParser
{
    /**
     * From api get sort string value with prefix '-' before field name, when need sort descending.
     *
     * @param string $sortString
     *
     * @return array
     */
    public function parseSortString(string $sortString): array
    {
        $result = [];

        if (substr($sortString, 0, 1) === '-') {
            $result['sortBy'] = substr($sortString, 1);
            $result['direction'] = 'desc';
        } else {
            $result['direction'] = 'asc';
            $result['sortBy'] = $sortString;
        }

        return $result;
    }
}