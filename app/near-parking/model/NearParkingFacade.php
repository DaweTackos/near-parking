<?php

namespace NearParking\Model;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use NearParking\Helper\GpsDistanceMeasure;

class NearParkingFacade
{

    const DISTANCE_UNIT = 'Km';

    /** @var EntityRepository */
    private $nearParkingRepository;

    /** @var GpsDistanceMeasure */
    private $gpsDistanceMeasure;


    public function __construct(EntityManager $em,
                                GpsDistanceMeasure $gpsDistanceMeasure)
    {
        $this->nearParkingRepository = $em->getRepository(ParkingEntity::class);
        $this->gpsDistanceMeasure = $gpsDistanceMeasure;
    }


    /**
     * @param float  $longitude
     * @param float  $latitude
     * @param string $sortBy
     * @param int    $page
     * @param int    $perPage
     * @param string $direction
     * @param array  $filters
     *
     * @return array
     */
    public function getNearParking(float $longitude,
                                   float $latitude,
                                   int $page,
                                   int $perPage,
                                   string $sortBy,
                                   string $direction = 'asc',
                                   array $filters = []) : array
    {
        $query = (new NearParkingQuery);

        foreach ($filters as $filter) {
            $query->filterBy($filter['column'], $filter['value']);
        }

        if ($sortBy !== 'distance') {
            $query->sortBy($sortBy, $direction);
        }

        $results = $this->nearParkingRepository->fetch($query);

        $offset = ($page - 1) * $perPage;
        $results->applyPaging($offset, $perPage);

        $results = $results->toArray(AbstractQuery::HYDRATE_ARRAY);
        $this->fillDistancesFromGps($longitude, $latitude, $results);

        if ($sortBy === 'distance') {
            usort($results, [$this, 'sortByDistance' . ucfirst($direction)]);
        }

        return $results;
    }


    /**
     * Fill distances to parking place results against gps coordinates.
     *
     * @param float $longitude
     * @param float $latitude
     * @param array $results
     */
    private function fillDistancesFromGps(float $longitude,
                                          float $latitude,
                                          array &$results)
    {
        foreach ($results as &$result) {
            $distance = $this->gpsDistanceMeasure->getDistance($latitude,
                                                               $longitude,
                                                               $result['latitude'],
                                                               $result['longitude'],
                                                               self::DISTANCE_UNIT);
            $result['distance'] = round($distance,2);
            $result['distanceUnit'] = self::DISTANCE_UNIT;
        }
        unset($result);
    }


    private function sortByDistanceAsc($a, $b)
    {
        if ($a['distance'] == $b['distance']) {
            return 0;
        }

        return ($a['distance'] < $b['distance']) ? -1 : 1;
    }


    private function sortByDistanceDesc($a, $b)
    {
        if ($a['distance'] == $b['distance']) {
            return 0;
        }

        return ($a['distance'] > $b['distance']) ? -1 : 1;
    }
}
