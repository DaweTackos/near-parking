<?php

namespace NearParking\Model;

use DateTime;
use Exception;
use Tracy\Debugger;
use Doctrine\ORM\EntityManager;

class ParkingSync
{

    const PARKING_API_URL = 'http://www.tsk-praha.cz/tskexport3/json/parkings';

    /** @var EntityManager $em */
    private $em;


    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }


    public function synchronizeParking(): array
    {
        $result = json_decode(file_get_contents(self::PARKING_API_URL), true);

        $inserted = $updated = 0;
        foreach ($result['results'] as $result) {
            try {
                /** @var ParkingEntity $parking */
                $parking = $this->em->find(ParkingEntity::class, $result['id']);
                $lastUpdated = (new DateTime)->setTimestamp($result['lastUpdated']);

                if ($parking) {
                    $parking->setGid($result['gid'])
                        ->setName($result['name'])
                        ->setLatitude($result['lat'])
                        ->setLongitude($result['lng'])
                        ->setFreePlaces($result['numOfFreePlaces'])
                        ->setTakenPlaces($result['numOfTakenPlaces'])
                        ->setTotalPlaces($result['totalNumOfPlaces'])
                        ->setPr($result['pr'])
                        ->setLastUpdated($lastUpdated)
                        ->setLastSynchronized((new DateTime));
                    $updated++;
                } else {
                    $parking = new ParkingEntity($result['id'],
                                                 $result['gid'],
                                                 $result['name'],
                                                 $result['lat'],
                                                 $result['lng'],
                                                 $result['numOfFreePlaces'],
                                                 $result['numOfTakenPlaces'],
                                                 $result['totalNumOfPlaces'],
                                                 $result['pr'],
                                                 $lastUpdated);
                    $inserted++;
                }

                $this->em->persist($parking);
            } catch (Exception $e) {
                Debugger::log($e, Debugger::ERROR);
            }
        }

        $this->em->flush();

        return [
            'task'     => 'Synchronize parking',
            'stats'    => [
                'inserted' => $inserted,
                'updated'  => $updated,
            ],
        ];
    }
}