<?php

namespace NearParking\Model;

use function bdump;
use Kdyby;
use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

class NearParkingQuery extends QueryObject
{
    /**
     * @var array|\Closure[]
     */
    private $filter = [];

    /**
     * @var array|\Closure[]
     */
    private $select = [];


    public function filterBy(string $column,
                             string $value): self
    {
        $this->filter[] = function (QueryBuilder $qb) use ($column, $value) {
            $qb->andWhere("p.$column LIKE :value")->setParameter('value', '%' . $value . '%');
        };

        return $this;
    }


    public function sortBy(string $column,
                           string $direction = 'asc')
    {
        $this->select[] = function (QueryBuilder $qb) use ($column, $direction) {
            $qb->addOrderBy("p.$column", $direction);
        };

        return $this;
    }


    /**
     * @param Queryable $repository
     *
     * @return QueryBuilder
     */
    protected function doCreateQuery(Queryable $repository): QueryBuilder
    {
        $qb = $this->createBasicDql($repository);

        foreach ($this->select as $modifier) {
            $modifier($qb);
        }

        return $qb;
    }


    protected function doCreateCountQuery(Queryable $repository)
    {
        return $this->createBasicDql($repository)->select('COUNT(p.id)');
    }


    private function createBasicDql(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('p')->from(ParkingEntity::class, 'p');

        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }

        return $qb;
    }
}