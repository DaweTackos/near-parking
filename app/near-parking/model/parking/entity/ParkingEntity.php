<?php

namespace NearParking\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="parking")
 */
class ParkingEntity
{

    /**
     * @var string $id
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $gid
     * @ORM\Column(type="integer")
     */
    private $gid;

    /**
     * @var string $name
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var float $latitude
     * @ORM\Column(type="float")
     */
    private $latitude;

    /**
     * @var float $longitude
     * @ORM\Column(type="float")
     */
    private $longitude;

    /**
     * @var int $freePlaces
     * @ORM\Column(type="integer")
     */
    private $freePlaces;

    /**
     * @var int $takenPlaces
     * @ORM\Column(type="integer")
     */
    private $takenPlaces;

    /**
     * @var int $totalPlaces
     * @ORM\Column(type="integer")
     */
    private $totalPlaces;

    /**
     * @var bool $pr
     * @ORM\Column(type="boolean")
     */
    private $pr;

    /**
     * @var DateTime $lastUpdated
     * @ORM\Column(type="datetime")
     */
    private $lastUpdated;

    /**
     * @var DateTime $lastSynchronized
     * @ORM\Column(type="datetime")
     */
    private $lastSynchronized;


    /**
     * ParkingEntity constructor.
     *
     * @param string   $id
     * @param string   $gid
     * @param string   $name
     * @param float    $latitude
     * @param float    $longitude
     * @param int      $freePlaces
     * @param int      $takenPlaces
     * @param int      $totalPlaces
     * @param bool     $pr
     * @param DateTime $lastUpdated
     */
    public function __construct(string $id,
                                string $gid,
                                string $name,
                                float $latitude,
                                float $longitude,
                                int $freePlaces,
                                int $takenPlaces,
                                int $totalPlaces,
                                bool $pr,
                                DateTime $lastUpdated)
    {
        $this->id = $id;
        $this->gid = $gid;
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->freePlaces = $freePlaces;
        $this->takenPlaces = $takenPlaces;
        $this->totalPlaces = $totalPlaces;
        $this->pr = $pr;
        $this->lastUpdated = $lastUpdated;
        $this->lastSynchronized = new DateTime;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getGid(): string
    {
        return $this->gid;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }


    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }


    /**
     * @return int
     */
    public function getFreePlaces(): int
    {
        return $this->freePlaces;
    }


    /**
     * @return int
     */
    public function getTakenPlaces(): int
    {
        return $this->takenPlaces;
    }


    /**
     * @return int
     */
    public function getTotalPlaces(): int
    {
        return $this->totalPlaces;
    }


    /**
     * @return bool
     */
    public function isPr(): bool
    {
        return $this->pr;
    }


    /**
     * @return DateTime
     */
    public function getLastUpdated(): DateTime
    {
        return $this->lastUpdated;
    }


    /**
     * @return DateTime
     */
    public function getLastSynchronized(): DateTime
    {
        return $this->lastSynchronized;
    }


    /**
     * @param string $gid
     *
     * @return ParkingEntity
     */
    public function setGid(string $gid): self
    {
        $this->gid = $gid;

        return $this;
    }


    /**
     * @param string $name
     *
     * @return ParkingEntity
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @param float $latitude
     *
     * @return ParkingEntity
     */
    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }


    /**
     * @param float $longitude
     *
     * @return ParkingEntity
     */
    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }


    /**
     * @param int $freePlaces
     *
     * @return ParkingEntity
     */
    public function setFreePlaces(int $freePlaces): self
    {
        $this->freePlaces = $freePlaces;

        return $this;
    }


    /**
     * @param int $takenPlaces
     *
     * @return ParkingEntity
     */
    public function setTakenPlaces(int $takenPlaces): self
    {
        $this->takenPlaces = $takenPlaces;

        return $this;
    }


    /**
     * @param int $totalPlaces
     *
     * @return ParkingEntity
     */
    public function setTotalPlaces(int $totalPlaces): self
    {
        $this->totalPlaces = $totalPlaces;

        return $this;
    }


    /**
     * @param bool $pr
     *
     * @return ParkingEntity
     */
    public function setPr(bool $pr): self
    {
        $this->pr = $pr;

        return $this;
    }


    /**
     * @param DateTime $lastUpdated
     *
     * @return ParkingEntity
     */
    public function setLastUpdated(DateTime $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }


    /**
     * @param DateTime $lastSynchronized
     *
     * @return ParkingEntity
     */
    public function setLastSynchronized(DateTime $lastSynchronized): self
    {
        $this->lastSynchronized = $lastSynchronized;

        return $this;
    }


}