<?php

namespace NearParkingModule\Presenters;

use Nette;
use NearParking\Helper\IPLocator;
use NearParking\Helper\SortApiParser;
use NearParking\Helper\FilterApiAdapter;
use NearParking\Model\NearParkingFacade;

class NearParkingApiPresenter extends Nette\Application\UI\Presenter
{

    /** @var NearParkingFacade @inject */
    public $nearParkingFacade;

    /** @var IPLocator @inject */
    public $ipLocator;

    /** @var SortApiParser @inject */
    public $sortApiParser;

    /** @var FilterApiAdapter @inject */
    public $filterApiAdapter;


    public function actionByGps(float $longitude,
                                float $latitude,
                                int $page,
                                int $perPage,
                                string $sort,
                                string $name = null)
    {
        $sort = $this->sortApiParser->parseSortString($sort);

        if (!empty($name)) {
            $filters[] = $this->filterApiAdapter->createFilterItem('name', $name);
        }

        $results = $this->nearParkingFacade->getNearParking($longitude,
                                                            $latitude,
                                                            $page,
                                                            $perPage,
                                                            $sort['sortBy'],
                                                            $sort['direction'],
                                                            $filters ?? []);

        $this->sendApiResult($results, $longitude, $latitude);
    }


    public function actionByClientIp(string $sort,
                                     int $page,
                                     int $perPage,
                                     string $name = null)
    {
        $sort = $this->sortApiParser->parseSortString($sort);
        $gpsData = $this->ipLocator->getGpsFromActualIP();

        if (!empty($name)) {
            $filters[] = $this->filterApiAdapter->createFilterItem('name', $name);
        }

        $results = $this->nearParkingFacade->getNearParking($gpsData['longitude'],
                                                            $gpsData['latitude'],
                                                            $page,
                                                            $perPage,
                                                            $sort['sortBy'],
                                                            $sort['direction'],
                                                            $filters ?? []);
        $this->sendApiResult($results, $gpsData['longitude'], $gpsData['latitude']);
    }


    private function sendApiResult(array $results,
                                   float $longitude,
                                   float $latitude): void
    {
        $this->sendJson([
                            'status'  => 'ok',
                            'parkingList' => $results,
                            'fromGps' => [
                                'latitude'  => $latitude,
                                'longitude' => $longitude,
                            ],
                        ]);
    }


    private function sendApiError(string $message, int $code): void
    {
        $this->sendJson([
            'status'  => 'error',
            'errorCode'    => $code,
            'message' => $message,
        ]);
    }

}
