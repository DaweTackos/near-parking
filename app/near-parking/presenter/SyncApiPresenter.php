<?php

namespace NearParkingModule\Presenters;

use NearParking\Model\ParkingSync;
use Nette;


class SyncApiPresenter extends Nette\Application\UI\Presenter
{

    /** @var ParkingSync @inject */
    public $parkingSync;


    public function actionSyncParking()
    {
        $result = $this->parkingSync->synchronizeParking();

        $this->sendJson($result);
    }

}
