Near-parking
=================

Application search nearest parking places from client ip-address position or gps location.

* Endpoint: **/api/near-parking/sync**
	* Method: GET
	* Return: json
	* Description: Synchronize parking places from external source to database.

* Endpoint: **api/near-parking/ip**
	* Parameters: **?sort=\<sort>&amp;name=\<name>&amp;page=\<page>&amp;perPage=\<perPage>**
	* Method: GET
	* Return: json
	* Description: Return list of parking places with distance from client ip-address location. 

* Endpoint: **api/near-parking/gps/\<longitude>,\<latitude>**
	* Parameters: **?sort=\<sort>&amp;name=\<name>&amp;page=\<page>&amp;perPage=\<perPage>**
	* Method: GET
	* Return: json
	* Description: Return list of parking places with distance from longitude,latitude location.


Requirements
------------

PHP 7.1 or higher.


Installation
------------

1. clone repository `git clone git@bitbucket.org:DaweTackos/near-parking.git`
2. rename file `app/config/config.local.neon.sample` to `app/config/config.local.neon` and fill database connection. 
3. in console go to root project folder
4. run `composer update`
5. run `php www/index.php orm:schema-tool:update --force`
6. fill parking places to database => run endpoint **/api/near-parking/sync** 


Near-parking schema
------------

![Near-parking schema](https://bitbucket.org/DaweTackos/near-parking/raw/0d74547e6e042ab34f9cd5e038983acd9b0c99c3/doc/near_parking-schema.png)